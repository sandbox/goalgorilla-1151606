(function ($) {
    $.fn.eventslotsWidget = function()
    {
        selectorStr = this;
        makeArray();
        $(selectorStr).bind("click", handleClick);
    };

    function handleClick(event)
    {		
    		var selectedRole = 'role-' + $('.role-select input:checked').val();
    		$(this).toggleClass('selected');	  
    		if (undefined != Drupal.settings.eventslots){   
    			for (role in Drupal.settings.eventslots.roles) {
						$(this).removeClass('role-' + role);
					}
				} else {
				$(this).addClass('role-0');
				}
				$(this).addClass(selectedRole);
				
				makeArray();
    };

		function makeArray(){
				var selectedArray = new Array();
				$('#edit-node-subscribe-data').text('');
				if (undefined != Drupal.settings.eventslots){
   				for (role in Drupal.settings.eventslots.roles) {
   				selectedArray[role] = new Array();
						$(".eventslots-cell.selected.role-" + role).each(function() {
							selectedArray[role].push($(this).attr('id').substr(4));
						});
						$('#edit-node-subscribe-data').text(JSON.stringify(selectedArray));
					}
				} else {
					  selectedArray[0] = new Array();
						$(".eventslots-cell.selected").each(function() {
							selectedArray[0].push($(this).attr('id').substr(4));
						});
					$('#edit-node-subscribe-data').text(JSON.stringify(selectedArray));
				}
		}

})(jQuery);

