<?php

//Hook_theme
function eventslots_theme($existing, $type, $theme, $path) {
    return array(
        'jquery_slots' => array(
            'arguments' => array('data' => null,'shiftlenght' => null, 'start' => null, 'end' => null, 'raw' => null)
    		),
    );
}

//Hook_theme
function theme_jquery_slots($data, $shiftlenght, $start, $end, $raw = null) {
	$data_array = json_decode($raw);
	if ($raw){
		if (is_array($data_array[0])){				
			foreach($data_array as $number => $role){
				foreach($role as $selected){
					$activate['id' . $selected] = ' selected role-' . $number;
				}
			}
		} else {
			foreach($data_array as $selected){
				$activate['id' . $selected] = ' selected';
			}
		}
	}

	$html = '<table id="eventslots-data-table">';
	for ($i = 0; $i <= 23; $i++) {
   	$headers[] = str_pad($i, 2, "0", STR_PAD_LEFT);
	}		

	foreach($headers as $th){
		$html .= '<th>' . $th . '</th>';	
	}			
	$i = 0;
	
	foreach($data as $number => $row){
		foreach($row['hours'] as $key => $cell){
			$cell_data = '';
			foreach((array)$cell as $cell_part){
				$timestamp_start = strtotime($start) + 7200 ;
				$timestamp_end = strtotime($end) + 7200;
				$timestamp_timeshift = strtotime(date('Y-m-d',$number) . ' ' . substr($key,0,3) . $cell_part . ':00');
				$class= '';
				if ($activate['id' . $i]){
					$class = $activate['id' . $i];
				}
				
				if ($timestamp_timeshift < $timestamp_start || $timestamp_timeshift >= $timestamp_end ){
					$class .= ' disabled';
				}				
				
				$cell_data .=  '<span id="esc-' . $i . '" title="' . date('H:i',$timestamp_timeshift) . '-' . date('H:i',strtotime("+$shiftlenght minutes", $timestamp_timeshift)) . '" class="eventslots-cell s' . $shiftlenght . $class . '">' . $cell_part . '</span>';
				$i++;
			}
			$data[$number]['html'] = $data[$number]['html'] . '<td class="eventslots-td">' . $cell_data . '</td>';
		}
		$html .= '<tr>' . $data[$number]['html'] . '</tr>';
	}
	$html .= '</table>';

	$header_html = '<table id="eventslots-sticky-header">';
	$header_html .= '<th>' . t('date') . '</th>';
	foreach($data as $number => $row){
		$header_html .= '<tr><td><strong>' . $row['datedisplay'] . ':</strong></td></tr>';
	}	
	$header_html .= '</table>';
	
	$html = $header_html . $html;	
	
	return $html;
}