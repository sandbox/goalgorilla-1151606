<?php

//Get the subscription node data
function get_subscribe_node($settings, $user_loaded = 'global'){
	//dpm($user_loaded);	
	
	if ($user_loaded == 'global'){
		global $user;
		$user_loaded = $user;
	}
	
	$sql = "SELECT node.nid AS nid
		FROM node node 
		INNER JOIN users users ON node.uid = users.uid
		INNER JOIN content_type_event_subscription node_data_field_event_ref ON node.vid = node_data_field_event_ref.vid
		WHERE (node.type in ('event_subscription')) AND (users.uid = %s) AND (node_data_field_event_ref.field_event_ref_nid = '%s')";
			
	$db_result = db_query($sql, $user_loaded->uid, $settings['nid']);
	$result = array();		
	while ($row = db_fetch_object($db_result)) {
		$result = $row;
	}	
		
	if($result->nid){
		$settings['subscription_nid'] = $result->nid;				
		$subscribe_node = node_load($settings['subscription_nid']);
	}

	return $subscribe_node;
}

//return array with 24 hours and sub times (15 or 30)
function get_hours($shiftlenght){
		if($shiftlenght == 15){
			for ($i = 0; $i <= 23; $i++) {
  	  	$hours[str_pad($i, 2, "0", STR_PAD_LEFT) . ':00']['00'] = '00';
  	  	$hours[str_pad($i, 2, "0", STR_PAD_LEFT) . ':00']['15'] = '15';
  	  	$hours[str_pad($i, 2, "0", STR_PAD_LEFT) . ':00']['30'] = '30';
  	  	$hours[str_pad($i, 2, "0", STR_PAD_LEFT) . ':00']['45'] = '45';
			}		
		} else if($shiftlenght == 30){
			for ($i = 0; $i <= 23; $i++) {
  	  	$hours[str_pad($i, 2, "0", STR_PAD_LEFT) . ':00']['00'] = '00';
  	  	$hours[str_pad($i, 2, "0", STR_PAD_LEFT) . ':00']['30'] = '30';
			}		
		} else if($shiftlenght == 60){
			for ($i = 0; $i <= 23; $i++) {
  	  	$hours[str_pad($i, 2, "0", STR_PAD_LEFT) . ':00']['00'] = '00';
			}		
		}
		return $hours;	
}

//Function to get days between two dates
function get_days($start, $end){
	$array = array();
	
	$calculated = date('d-m-Y',strtotime($start));

	$array[strtotime($calculated)] = $calculated;
	while (strtotime($calculated) <= strtotime($end)) {
		$calculated = date('d-m-Y' ,strtotime('next day',  strtotime($calculated)));
		if (strtotime($calculated) <= strtotime($end)){
			$array[strtotime($calculated)] = $calculated;
		}
  }	
  
	return $array;
}

//Process javascript array data
function process_data($data, $shiftlenght, $days, $roles){
	$data_array = json_decode($data);

	//Hier gebeurt iets lastigs
	//Namelijk het omzetten van de nummer array naar datum + begin en eind tijd
	$i = 0;
	foreach($days as $day_key => $day){
		$shifts_in_a_day = 60 / $shiftlenght * 24;
		$min = $i * $shifts_in_a_day;
		$max = $min + $shifts_in_a_day -1;
		$day_item = 0;

		foreach($data_array as $key => $role){
			$to_save[$day_key][$roles[$key]] = array();
			
			foreach($role as $item){
				if ($item >= $min && $item <= $max){
					$timeshift_of_day = $item - ($i * $shifts_in_a_day);
					$minutes = $timeshift_of_day * $shiftlenght;

					$to_save[$day_key][$roles[$key]][] = array('from' => minutes_to_time($minutes), 'to' => minutes_to_time($minutes + $shiftlenght));
				}	
			}
		}
	
		$i++;
	}  

	$counter = 0;

	foreach($to_save as $day_key => $day){
		foreach($day as $role_key => $role){
			foreach($role as $item_key => $item){
				//Start en er komt nog eentje aan
				if($to_save[$day_key][$role_key][$item_key -1]['to'] != $to_save[$day_key][$role_key][$item_key]['from'] && $to_save[$day_key][$role_key][$item_key +1]['from'] == $to_save[$day_key][$role_key][$item_key]['to']){
					$from = $item['from'];
				//Eind van een reeks
				} else if ($to_save[$day_key][$role_key][$item_key +1]['from'] != $to_save[$day_key][$role_key][$item_key]['to'] && $to_save[$day_key][$role_key][$item_key -1]['to'] == $to_save[$day_key][$role_key][$item_key]['from']){
					$to_save2[$day_key][$role_key][] = array('from' => $from, 'to' => $item['to']);
					$counter++;
				//Een enkele
				} else if ($to_save[$day_key][$role_key][$item_key -1]['to'] != $to_save[$day_key][$role_key][$item_key]['from'] && $to_save[$day_key][$role_key][$item_key +1]['from'] != $to_save[$day_key][$role_key][$item_key]['to']){
					$from = $item['from'];
					$to_save2[$day_key][$role_key][] = array('from' => $from, 'to' => $item['to']);
				}
			}
		}
	}

	return $to_save2;
}

//Minutes to Time
function minutes_to_time($minutes){
	$hours =  floor($minutes/60); // No. of mins/60 to get the hours and round down
	$mins =   $minutes % 60; // No. of mins/60 - remainder (modulus) is the minutes
	
	$hours = str_pad($hours, 2, "0", STR_PAD_LEFT);
	$mins = str_pad($mins, 2, "0", STR_PAD_LEFT);
	
	return $hours . ':' . $mins;
}

function eventslots_get_term_by_name($name, $vocabularies = null) {
  // multiple vocabularies to select from?
  if (is_array($vocabularies)) {
    $vocabs = ' AND t.vid IN (' . implode(', ', $vocabularies) . ') ';
  }
  // single vocabulary to select from?
  if (is_int($vocabularies)) {
    $vocabs = " AND t.vid = $vocabularies";
  }
  $db_result = db_query(db_rewrite_sql("SELECT t.tid, t.* FROM {term_data} t WHERE LOWER(t.name) = LOWER('%s')" . $vocabs, 't', 'tid'), trim($name));
  $result = array();
  while ($term = db_fetch_object($db_result)) {
    $result[] = $term;
  }
  return $result;
}

function eventslots_get_subscribed($nid){
	$sql = "SELECT node.nid AS nid
 FROM node node 
 LEFT JOIN content_type_event_subscription node_data_field_event_ref ON node.vid = node_data_field_event_ref.vid
 LEFT JOIN node node_node_data_field_event_ref ON node_data_field_event_ref.field_event_ref_nid = node_node_data_field_event_ref.nid
 INNER JOIN content_type_event_subscription node_data_field_gg_user_state ON node.vid = node_data_field_gg_user_state.vid
 WHERE (node_data_field_gg_user_state.field_gg_user_state_value = '1') AND (node.type in ('event_subscription')) AND (node_node_data_field_event_ref.nid = %s)";
			
	$db_result = db_query($sql, $nid);
	$result = array();		
	while ($row = db_fetch_object($db_result)) {
		$result[] = $row;
	}	
	
	return count($result);	
}

function eventslots_check_if_subscriptions($nid){
	$sql = "SELECT node.nid AS nid
 FROM node node 
 LEFT JOIN content_type_event_subscription node_data_field_event_ref ON node.vid = node_data_field_event_ref.vid
 LEFT JOIN node node_node_data_field_event_ref ON node_data_field_event_ref.field_event_ref_nid = node_node_data_field_event_ref.nid
 INNER JOIN content_type_event_subscription node_data_field_gg_user_state ON node.vid = node_data_field_gg_user_state.vid
 WHERE (node.type in ('event_subscription')) AND (node_node_data_field_event_ref.nid = '%s')";
			
	$db_result = db_query($sql, $nid);
	$result = array();		
	while ($row = db_fetch_object($db_result)) {
		$result[] = $row;
	}	

	//dpm($result);

	if($result){
		return true;	
	} else {
		return false;
	}
}

function eventslots_user_subscribed($nid, $user_loaded = 'global'){
	if ($user_loaded == 'global'){
		global $user;
		$user_loaded = $user;
	}
	
	$sql="SELECT node.nid AS nid
 FROM node node 
 LEFT JOIN content_type_event_subscription node_data_field_event_ref ON node.vid = node_data_field_event_ref.vid
 LEFT JOIN node node_node_data_field_event_ref ON node_data_field_event_ref.field_event_ref_nid = node_node_data_field_event_ref.nid
 INNER JOIN content_type_event_subscription node_data_field_gg_user_state ON node.vid = node_data_field_gg_user_state.vid
 INNER JOIN users users ON node.uid = users.uid
 WHERE (node_data_field_gg_user_state.field_gg_user_state_value = '1') AND (node.type in ('event_subscription')) AND (node_node_data_field_event_ref.nid = '%s') AND (users.uid = '%s')";

	$db_result = db_query($sql, $nid, $user_loaded->uid);
	$result = array();		
	while ($row = db_fetch_object($db_result)) {
		$result[] = $row;
	}	
	
	if($result){
		return true;	
	} else {
		return false;
	}
}