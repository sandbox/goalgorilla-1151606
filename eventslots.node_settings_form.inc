<?php

//Callback for menu
function eventslots_create_form($form_state, $node){
	
	$form = array();	
	
  ahah_helper_register($form, $form_state);	

  if ($form_state['submitted'] == false){
		//Checking if there is already data for this node
		$result = db_query('SELECT eid, nid, type, value from {eventslots_nids} WHERE nid = "%s"',$node->nid);
  	if (count($result) > 0) {
    	$data = db_fetch_array($result);
    	if ($data != FALSE) {
				//Setting default values with older data
				$form_state['storage'] = unserialize($data['value']);
    	}
  	}
	}
	if (!isset($form_state['storage']['quantity'])) {
		$quantity = 1;
	} else {
		$quantity = $form_state['storage']['quantity'];
		if ($form_state['clicked_button']['#id'] == 'edit-shifts-roles-submit') {
			$quantity++;
		}
	}
	
	$form['nid'] = array(
		'#type' => 'value',
		'#value' => $node->nid,
	);

	//This is for the add another role
	$form['quantity'] = array(
		'#type' => 'value',
		'#value' => $quantity,
	);

	//Shifts settings
	
  $form['shifts'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Event settings'),
  	'#description' => t('Please fill in the settings below.'),
    '#prefix' => '<div id="shifts-wrapper">', // This is our wrapper div.
    '#suffix' => '</div>',
    '#tree'   => TRUE, // Don't forget to set #tree!
  );	
	
	$form['shifts']['shifts_attend'] = array(
  	'#type' => 'checkbox',
	  '#default_value' => $form_state['storage']['shifts']['shifts_attend'],
  	'#title' => t('People can be submitted and/or submit  themselves to this event'),
    '#ahah' => array(
    	'event' => 'change',
      'path' => ahah_helper_path(array('shifts')),
      'wrapper' => 'shifts-wrapper',
      'method' => 'replace',
      'effect' => '',
    ),
	);	

	if ($form_state['storage']['shifts']['shifts_attend'] == 1){	
	
		if ($form_state['storage']['shifts']['shifts_check']) {
			$default = $form_state['storage']['shifts']['shifts_check'];
		} else {
			$default = 0;	
		}	
	
		$form['shifts']['shifts_check'] = array(
  		'#type' => 'radios',
  		'#title' => t('Please select'),
	  	'#default_value' => $default,
  		'#options' => array(t('Only subscribe'), t('This event has time shifts'), t('This event has a minimum and/or maximum of people to attend')),
    	'#ahah' => array(
    		'event' => 'change',
      	'path' => ahah_helper_path(array('shifts')),
      	'wrapper' => 'shifts-wrapper',
      	'method' => 'replace',
      	'effect' => '',
    	),
    	'#required' => TRUE,
		);		

		if ($form_state['storage']['shifts']['shifts_check'] == 1){
	
		$form['shifts']['shift_length'] = array(
  		'#type' => 'select',
  		'#title' => t('Lenght of a shift'),
	 	  '#options' => array(
		    '15' => t('15 minutes'),
    		'30' => t('30 minutes'),
    		'60' => t('60 minutes'),
	  	),
  		'#multiple' => false,
  		'#default_value' => $form_state['storage']['shifts']['shift_length'],
  		'#size' => 10,
	  	'#maxlength' => 128,
  		'#field_suffix' => t('minutes'),
  		'#required' => TRUE,
		);

		//Role settings

	  $form['shifts']['roles'] = array(
  	 	'#type'   => 'fieldset',
    	'#title'  => t('Role settings'),
  		'#description' => t('Roles are made with the settings below.'),
  	 	'#prefix' => '<div id="roles-wrapper">', // This is our wrapper div.
	   	'#suffix' => '</div>',
    	'#tree'   => TRUE, // Don't forget to set #tree!
  	);	
	
		$form['shifts']['roles']['roles_check'] = array(
	 		'#type' => 'checkbox',
		  '#default_value' => $form_state['storage']['shifts']['roles']['roles_check'],
  		'#title' => t('This event has roles.'),
	   	'#ahah' => array(
   			'event' => 'change',
      	'path' => ahah_helper_path(array('shifts','roles')),
     		'wrapper' => 'roles-wrapper',
     		'method' => 'replace',
     		'effect' => '',
	   	),
    );

   	if ($form_state['storage']['shifts']['roles']['roles_check'] == 1){
			$form['shifts']['roles']['role_items'] = array(
   			'#type'   => 'fieldset',
   			'#title'  => t('Create roles'),
   			'#prefix' => '<div id="role-items-wrapper">', // This is our wrapper div.
   			'#suffix' => '</div>',
   			'#tree'   => TRUE, // Don't forget to set #tree!
	 		);	    		
  		
		$form['shifts']['roles']['role_items']['prefix'] = array(		
   		'#value' => '<table><th>' . t('role title') . '</th><th>' . t('description') . '</th><th>' . t('min') . '</th><th>' . t('max') . '</th><th>' . t('reserve') . '</th>',
   	);    		

		for ($i = 1; $i <= $quantity; $i++) {
	   	$form['shifts']['roles']['role_items'][$i]['role_title'] = array(
  			'#type' => 'textfield',
  			'#default_value' => $form_state['storage']['shifts']['roles']['role_items'][$i]['role_title'],
  			'#size' => 16,
				'#prefix' => '<tr><td>',
        '#suffix' => '</td>',
  			'#maxlength' => 128,
			);

  		$form['shifts']['roles']['role_items'][$i]['description'] = array(
 				'#type' => 'textfield',
 				'#default_value' => $form_state['storage']['shifts']['roles']['role_items'][$i]['description'],
  			'#size' => 40,
 				'#prefix' => '<td>',
   	    '#suffix' => '</td>',
 				'#maxlength' => 128,
			);			

   		$form['shifts']['roles']['role_items'][$i]['min'] = array(
 				'#type' => 'textfield',
 				'#default_value' => $form_state['storage']['shifts']['roles']['role_items'][$i]['min'],
 				'#size' => 10,
 				'#prefix' => '<td>',
       	'#suffix' => '</td>',
 				'#maxlength' => 4,
			);	
			
   		$form['shifts']['roles']['role_items'][$i]['max'] = array(
 				'#type' => 'textfield',
 				'#default_value' => $form_state['storage']['shifts']['roles']['role_items'][$i]['max'],
 				'#size' => 10,
 				'#prefix' => '<td>',
       	'#suffix' => '</td>',
 				'#maxlength' => 4,
			);	

   		$form['shifts']['roles']['role_items'][$i]['reserve'] = array(
 				'#default_value' => $form_state['storage']['shifts']['roles']['role_items'][$i]['reserve'],
  			'#type' => 'checkbox',
 				'#prefix' => '<td>',
       	'#suffix' => '</td></tr>',
			);				
									
   	}

		$form['shifts']['roles']['role_items']['suffix'] = array(		
			'#value' => '</table>',
 		);
    	
   	$form['shifts']['roles']['submit'] = array(
   		'#type' => 'submit',
   		'#value' => t('Add another role'),
    	'#ahah' => array(
    		'event' => 'click',
 	    	'path' => ahah_helper_path(array('shifts','roles','role_items')),
   	  	'wrapper' => 'role-items-wrapper',
     		'method' => 'replace',
     		'effect' => '',
   		),
   	);
 	}
}	

	if ($form_state['storage']['shifts']['shifts_check'] == 2){
		$form['shifts']['other'] = array(
   		'#type'   => 'fieldset',
   		'#title'  => t('Maximum and/or minimum settings'),
  		'#description' => t('Please fill in the settings below'),
   		'#prefix' => '<div id="maxmin-wrapper">', // This is our wrapper div.
   		'#suffix' => '</div>',
   		'#tree'   => TRUE, // Don't forget to set #tree!
  	);			

		$form['shifts']['other']['prefix'] = array(		
  		'#value' => '<table><th>' . t('min') . '</th><th>' . t('max') . '</th>',
  	);  
   					
		$form['shifts']['other']['min'] = array(
  		'#type' => 'textfield',
  		'#default_value' => $form_state['storage']['shifts']['other']['min'],
  		'#size' => 10,
  		'#prefix' => '<tr><td>',
     	'#suffix' => '</td>',
  		'#maxlength' => 4,
		);	
			
   	$form['shifts']['other']['max'] = array(
  		'#type' => 'textfield',
  		'#default_value' => $form_state['storage']['shifts']['other']['max'],
  		'#size' => 10,
  		'#prefix' => '<td>',
     	'#suffix' => '</td></tr>',
  		'#maxlength' => 4,
		);	

		$form['shifts']['other']['suffix'] = array(		
  		'#value' => '</table>',
   	);   
	}
}
	
$form['submit'] = array('#type' => 'submit', '#value' => t('Save event settings'));	

//When there are subscriptions disable some settings and show info	
if (eventslots_check_if_subscriptions($node->nid) && $form_state['submitted'] == false){
	$message = t('Sorry, you can not change some of the settings for this event. People have already subscribed.');
	drupal_set_message( $message ,'error') ;	
	if(user_access('delete all subscriptions')) {
		$message2 = ' ' . t('It seems you have the right rights! You can delete all subscriptions ') . '<a href="' . base_path() . 'node/' . $node->nid . '/delete-event-subscriptions">' . t('here') . '</a>'; 
		drupal_set_message( $message2 ,'status') ;	
	}
}
if (eventslots_check_if_subscriptions($node->nid)){
	//Disabling stuff
	$form['shifts']['shifts_attend']['#disabled'] = true;	
	$form['shifts']['shifts_check']['#disabled'] = true;	
	$form['shifts']['shift_length']['#disabled'] = true;	
	$form['shifts']['roles']['roles_check']['#disabled'] = true;
	/*
	foreach($form['shifts']['roles']['role_items'] as $key => $item){
		if(count($form['shifts']['roles']['role_items'][$key]) == 5){
			if ($key <= $form_state['storage']['quantity']){
				$form['shifts']['roles']['role_items'][$key]['role_title']['#disabled'] = true;
				$form['shifts']['roles']['role_items'][$key]['description']['#disabled'] = true;		
				$form['shifts']['roles']['role_items'][$key]['min']['#disabled'] = true;		
				$form['shifts']['roles']['role_items'][$key]['max']['#disabled'] = true;	
				$form['shifts']['roles']['role_items'][$key]['reserve']['#disabled'] = true;							
			}
		}
	}	*/
	
} else {
	//drupal_set_message( t('You can not change settings after someone has subscribed.') ,'status') ;			
}	
	
return $form;
}

function eventslots_create_form_submit($form, &$form_state) {
	if($form_state['clicked_button']['#id'] == 'edit-submit'){
		
	//dpm($form_state);		
		
		if ($form_state['values']['shifts']['roles']['role_items']){
			foreach($form_state['values']['shifts']['roles']['role_items'] as $key => $role){
				if ($key != 1){
					if ($role['role_title'] == null && $role['description'] == null && $role['min'] == null && $role['max'] == null && $role['reserve'] == 0){
						unset($form_state['values']['shifts']['roles']['role_items'][$key]);	
					}
				}
			}
		}
		$form_state['values']['quantity'] = count($form_state['values']['shifts']['roles']['role_items']); 

		$result['nid'] = $form_state['values']['nid'];
		$result['value'] = serialize($form_state['values']);

		//dpm($result);

		if($eid = db_result(db_query('SELECT eid from {eventslots_nids} WHERE nid = "%s"',$result['nid']))) {
			$result['eid'] = $eid;
    	$results = drupal_write_record('eventslots_nids', $result, array('eid')); // Do Update
		} else {
    	$results = drupal_write_record('eventslots_nids', $result); // Do insert
		}
	
		if($results == 1 || $results == 2){
			drupal_set_message(t('Your event settings have been saved'));	
		} else {
			drupal_set_message(t('We are sorry, there has been an error'));	
		}
	}
}