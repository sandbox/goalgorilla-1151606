<?php

//Form to subscribe
function eventslots_node_subscribe_form($form_state, $settings){
	//dpm($form_state);	
	
	$form_state['storage']['settings'] = $settings;
	$form = array();	
	
	global $user;			

	if (user_access('subscribe others to event')){	
		if ($form_state['post']['select_user']){
			$user_loaded = user_load(array('name' => check_plain($form_state['post']['select_user'])));
			$_SESSION['user_loaded'] = $user_loaded;				
		} else if ($_SESSION['user_loaded'] != null){
			$user_loaded = $_SESSION['user_loaded'];
		}	else {
			$user_loaded = $user;
			$_SESSION['user_loaded'] = $user_loaded;				
		}

	//dpm($_SESSION['user_loaded']);

		$form['select_user'] = array(
	  	'#type' => 'textfield',
  		'#title' => t('The user you want to apply the subscription to'),
  		'#default_value' => $_SESSION['user_loaded']->name,
  		'#size' => 60,
  		'#maxlength' => 128,
 		  '#autocomplete_path' => 'user/autocomplete',
  		'#required' => TRUE,
 		  '#ahah' => array(
  			'event' => 'change',
	  		'path' => ahah_helper_path(array('node_subscribe')),
	 			'wrapper' => 'node-subscribe-wrapper',
 				'method' => 'replace',
  			'effect' => '',
			),
		);
	} else {
		$user_loaded = $user;
		$_SESSION['user_loaded'] = $user_loaded;				
	}
	//dpm($user_loaded);
	$subscribe_node = get_subscribe_node($settings, $user_loaded);
	//dpm($subscribe_node);	
	
	
	if ($form_state['submitted'] == true && $form_state['values']['type'] == 1){

		if ($subscribe_node == ''){
			$new = true;
			$subscribe_node = (object) NULL;
			$subscribe_node->type = 'event_subscription';
			$subscribe_node->title = 'Content subscription by '. $user_loaded->name . ' for ' . $settings['nid'];
			$subscribe_node->uid = $user_loaded->uid;
			$subscribe_node->created = strtotime("now");
			$subscribe_node->changed = strtotime("now");
			$subscribe_node->status = 1;
			$subscribe_node->comment = 0;
			$subscribe_node->promote = 0;
			$subscribe_node->moderate = 0;
			$subscribe_node->sticky = 0;
			// add CCK field data
			$subscribe_node->field_event_ref[0]['nid'] = $settings['nid'];
		}
			unset($subscribe_node->field_gg_timeslots);
			$subscribe_node->field_gg_raw_data[0]['value'] = null;
			$subscribe_node->field_gg_timeslots[0]['value'] = null;
			$subscribe_node->field_gg_timeslots[0]['value2'] = null;
			$subscribe_node->field_gg_user_state[0]['value'] = $form_state['values']['node_subscribe']['options'];

			$result = node_save($subscribe_node);

			drupal_set_message('Your subscription is saved');
	}	
	
	if ($settings['shifts']['shifts_attend'] == 1 && $settings['shifts']['shifts_check'] == 0 || $settings['shifts']['shifts_attend'] == 1 && $settings['shifts']['shifts_check'] == 2) {
	$form_state['storage']['node_subscribe']['options'] = $subscribe_node->field_gg_user_state[0]['value'];		

 	drupal_add_css(drupal_get_path('module', 'eventslots') .'/css/eventslots.css');  		
  $form['node_subscribe'] = array(
 		'#type'   => 'fieldset',
		'#prefix' => '<div id="node-subscribe-wrapper">', // This is our wrapper div.
 	  '#suffix' => '</div>',
		'#tree'   => TRUE, // Don't forget to set #tree!
	);	

	$subscribed = eventslots_get_subscribed($settings['nid']);
		
	if ($settings['shifts']['shifts_attend'] == 1 && $settings['shifts']['shifts_check'] == 2) {		
		
		$form['node_subscribe']['options'] = array(
			'#type' => 'checkbox',
			'#title' => t('I will be there'),
			'#default_value' => $form_state['storage']['node_subscribe']['options'],
 		  '#ahah' => array(
  			'event' => 'change',
	  		'path' => ahah_helper_path(array('node_subscribe')),
	 			'wrapper' => 'node-subscribe-wrapper',
 				'method' => 'replace',
  			'effect' => '',
			),
		);
		
	} else {
		$form['node_subscribe']['options'] = array(
			'#type' => 'radios',
			'#options' => array('1' => t('I will be there'),'0' => t('I will maybe be there'),'2' => t('I will not be there')),
			'#default_value' => $form_state['storage']['node_subscribe']['options'],
 		  '#ahah' => array(
  			'event' => 'change',
	  		'path' => ahah_helper_path(array('node_subscribe')),
	 			'wrapper' => 'node-subscribe-wrapper',
 				'method' => 'replace',
  			'effect' => '',
			),
		);
	}

	$form['node_subscribe']['subscribed'] = array(
	  '#value' => '<p id="eventslots-subscribed-people">' . t('People subscribed: ') . '<span class="number">' . $subscribed . '</span></p>',
	);

	if($settings['shifts']['other']['max'] == $subscribed){
		$form['node_subscribe']['full'] = array(
		  '#value' => '<p id="eventslots-full-people">' . t('This event is full!') . '</p>',
		);
		if (!eventslots_user_subscribed($settings['nid'], $user_loaded)){
			$form['node_subscribe']['options']['#disabled'] = true;
		}
	}

	if($settings['shifts']['other']['more'] > $subscribed){
		$form['node_subscribe']['more'] = array(
		  '#value' => '<p id="eventslots-more-people">' . t('Please subscribe, we need more people!') . '</p>',
		);
	}		

	if($settings['shifts']['other']['min']){
		$form['node_subscribe']['min'] = array(
		  '#value' => '<p id="eventslots-min-people">' . t('Minimum of people needed: ') . '<span class="number">' . $settings['shifts']['other']['min'] . '</span></p>',
		);
	}		

	if($settings['shifts']['other']['max']){
		$form['node_subscribe']['max'] = array(
		  '#value' => '<p id="eventslots-max-people">' . t('Maximum of people allowed: ') . '<span class="number">' . $settings['shifts']['other']['max'] . '</span></p>',
		);
	}		

	$form['type'] = array('#type' => 'value', '#value' => 1);
		
	return $form;
}	

	if ($settings['shifts']['shifts_attend'] == 1 && $settings['shifts']['shifts_check'] == 1) {

		//Adding our css and js files
  	drupal_add_css(drupal_get_path('module', 'eventslots') .'/css/eventslots.css');  
	  drupal_add_js(drupal_get_path('module', 'eventslots') .'/js/jquery.eventslotswidget.js');  				
	  drupal_add_js(drupal_get_path('module', 'eventslots') .'/js/jquery.mousewheel.js');  			
	  drupal_add_js(drupal_get_path('module', 'eventslots') .'/js/json2.js');  			
	  drupal_add_js(drupal_get_path('module', 'eventslots') .'/js/eventslots.js');  			

		//Init
		$event_node = node_load($settings['nid']);
		$hours = get_hours($settings['shifts']['shift_length']);
		$days = get_days($event_node->field_date[0]['value'],$event_node->field_date[0]['value2']);
		$subscribe_node = get_subscribe_node($settings, $user_loaded);
		foreach($days as $key => $day){
			$days[$key] = array('datedisplay' => $days[$key], 'hours' => $hours);
		}			
				
		//If the event has shifts and roles
		if($settings['shifts']['roles']['roles_check'] == 1){
			//Init settings
			foreach($settings['shifts']['roles']['role_items'] as $role){
				$roles[] = $role['role_title'];
			}
			
			$javascript_settings = array(
  			'roles' => $roles,
  			'shiftlenght' => $settings['shifts']['shift_length'],
			);

			drupal_add_js(array('eventslots' => $javascript_settings), 'setting');
			
			$form['select_role'] = array(
  			'#type' => 'radios',
  			'#title' => t('Please select a role'),
  			'#default_value' => 0,
  			'#options' => $roles,
  			'#attributes' => array('class' => 'role-select'),
  			'#weight' => -1,
			);
		}

		//This is our wrapper
	  $form['node_subscribe'] = array(
  		'#type'   => 'fieldset',
 			'#prefix' => '<div id="node-subscribe-wrapper">', 
  	  '#suffix' => '</div>',
 			'#tree'   => TRUE, 
	  );	

		//Important element!
		//This is the timeslots widget
		$form['node_subscribe']['hours-' . $day] = array(
  		'#value' => theme('jquery_slots', $days,$settings['shifts']['shift_length'], $event_node->field_date[0]['value'],$event_node->field_date[0]['value2'], $subscribe_node->field_gg_raw_data[0]['value']),
		);

	  if( module_exists('beautytips') ) {
  		$options['eventslots'] = array(
				'cssSelect' => '.eventslots-cell:not(".disabled")',
  			'style' => 'eventslots',
  		);
  		beautytips_add_beautytips($options);
  	}
	

		//This is for submit handler, we tell that we are type 2 
		//which means the event has timeshifts and roles
		$form['type'] = array('#type' => 'value', '#value' => 2);

		//Here we will save the javascript array
		$form['node_subscribe']['data'] = array(
  		'#type' => 'textarea',
  		'#title' => t('Data'),
		);
		
		$form['submit'] = array('#type' => 'submit', '#value' => t('Save the subscription'));

		return $form;
 	}	
}

//Form submit
function eventslots_node_subscribe_form_submit($form, &$form_state) {	
	//dpm($form_state);	
	
	$settings = $form_state['storage']['settings'];

	$user_loaded = $_SESSION['user_loaded'];		

	//dpm($settings);

	$subscribe_node = get_subscribe_node($settings, $user_loaded);
	
	
	$event_node = node_load($settings['nid']);	
	
	if($settings['shifts']['roles']['role_items']){
		foreach($settings['shifts']['roles']['role_items'] as $role){ 
			$roles[] = $role['role_title'];
		}
	} else {
		$roles[] = 'default';	
	}

	if ($form_state['submitted'] == true && $form_state['values']['type'] == 2){
		$days = get_days($event_node->field_date[0]['value'],$event_node->field_date[0]['value2']);	
		
		$processed = process_data($form_state['values']['node_subscribe']['data'], $settings['shifts']['shift_length'], $days, $roles);

		foreach($roles as $role){
			if (eventslots_get_term_by_name($role,variable_get('eventslots_taxvoc', 0)) == null) {
        $edit = array('vid' => variable_get('eventslots_taxvoc', 0),'name' => $role);
        taxonomy_save_term($edit);
			}
		}

		if ($subscribe_node == ''){
			$new = true;
			//If there is no data in settings[subscription_node] it means 
			//the user has not saved the subscription form before
			$subscribe_node = (object) NULL;
			$subscribe_node->type = 'event_subscription';
			$subscribe_node->title = 'Content subscription by '. $user_loaded->name . ' for ' . $settings['nid'];
			$subscribe_node->uid = $user_loaded->uid;
			$subscribe_node->created = strtotime("now");
			$subscribe_node->changed = strtotime("now");
			$subscribe_node->status = 1;
			$subscribe_node->comment = 0;
			$subscribe_node->promote = 0;
			$subscribe_node->moderate = 0;
			$subscribe_node->sticky = 0;
			// add CCK field data
		}

		$subscribe_node->field_gg_raw_data[0]['value'] = $form_state['values']['node_subscribe']['data'];
		$subscribe_node->field_event_ref[0]['nid'] = $settings['nid'];
		
		$subscribe_node->field_gg_roles = null;		
		$subscribe_node->field_gg_timeslots = null;

		if ($processed){
			$subscribe_node->field_gg_user_state[0]['value'] = 0;
			foreach($processed as $day_key => $day){
				foreach($day as $role_key => $role){
					foreach($role as $timeset){
						$tax = eventslots_get_term_by_name($role_key, variable_get('eventslots_taxvoc', 0));
						$subscribe_node->field_gg_roles[]['value'] = $tax[0]->tid;
						$subscribe_node->field_gg_timeslots[] = array('value' => date('Y-m-d',$day_key) . ' ' . $timeset['from'], 'value2' => date('Y-m-d',$day_key) . ' ' . $timeset['to']);
					}
				}
			}
		} else {		
			$subscribe_node->field_gg_timeslots[] = array('value' => null, 'value2' => null);
			$subscribe_node->field_gg_roles[] = array('value' => null);
			$subscribe_node->field_gg_user_state[0]['value'] = 2;
		}
		
		$result = node_save($subscribe_node);
		drupal_set_message('Your subscription is saved');
		drupal_goto('node/' . arg(1));
	}	

}
